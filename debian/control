Source: lomiri-action-api
Section: libs
Priority: optional
Maintainer: Debian UBports Team <team+ubports@tracker.debian.org>
Uploaders: Mike Gabriel <sunweaver@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               libglib2.0-dev,
               pkg-config,
               qt5-qmake,
               qtbase5-dev,
               qttools5-dev-tools,
               qtdeclarative5-dev,
               qtdeclarative5-dev-tools,
               qml-module-qtquick2,
               qml-module-qttest,
               dbus-test-runner,
               doxygen,
               graphviz,
               rdfind,
               symlinks,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://gitlab.com/ubports/development/core/lomiri-action-api
Vcs-Git: https://salsa.debian.org/ubports-team/lomiri-action-api.git
Vcs-Browser: https://salsa.debian.org/ubports-team/core/lomiri-action-api

Package: liblomiri-action-qt1
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: Lomiri Action Qt API
 Lomiri Common Action API. Allow applications to export actions in
 various forms to the Lomiri Shell.
 .
 This package provides the Qt library of the API.

Package: liblomiri-action-qt1-dev
Section: libdevel
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         liblomiri-action-qt1 (= ${binary:Version}),
         qtbase5-dev,
Suggests: lomiri-action-doc,
Description: Lomiri Action Qt API - development files
 Lomiri Common Action API. Allow applications to export actions in
 various forms to the Lomiri Shell.
 .
 This package contains development files to develop against the library.

Package: qml-module-lomiri-action
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         liblomiri-action-qt1 (= ${binary:Version}),
Description: Lomiri Action QML Components
 Lomiri Common Action API. Allow applications to export actions in
 various forms to the Lomiri Shell.
 .
 This package contains the qtdeclarative binding for the lomiri-action
 library.

Package: lomiri-action-doc
Section: doc
Architecture: all
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: Lomiri Action API - documentation
 Lomiri Common Action API. Allow applications to export actions in
 various forms to the Lomiri Shell.
 .
 This package contains developer documentation.
